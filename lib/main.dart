import 'package:flutter/material.dart';
import 'package:thalia/bloc/home/home_page.dart';
import 'package:thalia/db/dbInitHelper.dart';
import 'package:thalia/models/meddicine.dart';
import 'db/dbInitHelper.dart';
import 'models/pet.dart';
import 'models/disease.dart';

void main() {
  DBInitHelper helper = DBInitHelper(dbTablesName: [
    'pets',
    'meddicine',
    'disease'
  ], tablesField: [
    Pet.getFieldsForDB(),
    Meddicine.getFieldsForDB(),
    Disease.getFieldsForDB(),
  ]);
  runApp(MyApp());
  helper.initDb();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Color(0x426e6f).withOpacity(1),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}
