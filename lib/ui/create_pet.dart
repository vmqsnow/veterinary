import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';
import 'package:thalia/models/pet.dart';

import 'components/gender_select.dart';

class CreatePetSheet extends StatefulWidget {
  bool loading;
  CreatePetSheet({
    Key key,
    this.loading = false,
  }) : super(key: key);

  @override
  _CreatePetSheetState createState() => _CreatePetSheetState();
}

class _CreatePetSheetState extends State<CreatePetSheet> {
  bool selectedGender = true;
  bool species = true;

  //Tetfield controllers
  TextEditingController nameController = TextEditingController();
  TextEditingController ownerController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController fixedPhoneController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController speciesController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  List<String> options = <String>['Perro', 'Gato'];
  String dropdownValue = 'Gato';
  bool loading;

  @override
  void initState() {
    loading = widget.loading;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .9,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(top: 20),
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    child: Text("Cancelar",
                        style: TextStyle(
                          color: Colors.blue,
                        )),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Text(
                    "Nueva mascota",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    onTap: () {
                      if (formKey.currentState.validate()) {
                        Pet pet = Pet(
                          name: nameController.value.text,
                          owner: ownerController.value.text,
                          phoneNumber: phoneController.value.text,
                          fixedphoneNumber: fixedPhoneController.value.text,
                          breed: breedController.value.text,
                          gender: selectedGender,
                          birthDate: DateTime.now().subtract(
                            Duration(
                              days: 365 *
                                  int.parse(
                                    ageController.value.text.isEmpty
                                        ? "0"
                                        : ageController.value.text.isEmpty,
                                  ),
                            ),
                          ),
                          speciesId: options.indexOf(dropdownValue),
                          id: DateTime.now().millisecondsSinceEpoch,
                        );
                        PetCreateBloc().add(StorePetEvent(
                          pet: pet,
                          context: context,
                        ));
                        setState(() {
                          loading = true;
                        });
                      }
                    },
                    child: Text(
                      "Aceptar",
                      style: TextStyle(
                        color: loading ? Colors.grey : Colors.blue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      right: 10,
                      top: 25,
                      child: Opacity(
                        opacity: 0.05,
                        child: Image.asset(
                          dropdownValue == 'Gato'
                              ? "assets/cat64.png"
                              : "assets/dog64.png",
                          scale: 0.7,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              CircleAvatar(
                                backgroundColor: Colors.grey.shade300,
                                radius: 60,
                                child: Text(
                                  nameController.text.isNotEmpty
                                      ? nameController.text[0].toUpperCase()
                                      : "",
                                  style: TextStyle(
                                    fontSize: 40,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 10,
                                bottom: 0,
                                child: Container(
                                  height: 30,
                                  padding: const EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                          color: Colors.grey,
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          offset: Offset(0, 1),
                                        )
                                      ]),
                                  child: Image.asset(
                                    selectedGender
                                        ? "assets/male.png"
                                        : "assets/female.png",
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5),
                            child: InkWell(
                              child: Text("Agregar foto",
                                  style: TextStyle(
                                    color: Colors.blue,
                                  )),
                              onTap: () {},
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 20,
                            ),
                            child: Column(
                              children: <Widget>[
                                inputWidget("Nombre:",
                                    controller: nameController),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          30,
                                      child: inputWidget("Edad:",
                                          controller: ageController,
                                          inputType: TextInputType.number,
                                          validator: (value) {
                                        if (value.isNotEmpty &&
                                            int.tryParse(value) == null) {
                                          return "El valor no es válido";
                                        }
                                        return null;
                                      }),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          30,
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 5,
                                      ),
                                      child: genderWidget(context),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Text(
                                        "Especie:",
                                        style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    DropdownButton<String>(
                                      value: dropdownValue,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          dropdownValue = newValue;
                                        });
                                      },
                                      style: TextStyle(color: Colors.blue),
                                      selectedItemBuilder:
                                          (BuildContext context) {
                                        return options.map((String value) {
                                          return Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: Image.asset(
                                                    value == "Gato"
                                                        ? "assets/cat64.png"
                                                        : "assets/dog64.png",
                                                    height: 20,
                                                  ),
                                                ),
                                                Text(
                                                  dropdownValue,
                                                  style: TextStyle(
                                                      color: Colors.black),
                                                  textAlign: TextAlign.end,
                                                ),
                                              ]);
                                        }).toList();
                                      },
                                      items: options
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                                inputWidget(
                                  "Raza:",
                                  controller: breedController,
                                  validator: (value) => null,
                                ),
                                inputWidget("Dueño:",
                                    controller: ownerController),
                                inputWidget(
                                  "Teléfono:",
                                  controller: phoneController,
                                  validator: (value) => null,
                                  prefix: Text("+53"),
                                ),
                                inputWidget(
                                  "Fijo:",
                                  controller: fixedPhoneController,
                                  validator: (value) => null,
                                  prefix: Text("+53"),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget inputWidget(
    String title, {
    TextEditingController controller,
    TextInputType inputType = TextInputType.text,
    String Function(String value) validator,
    Widget prefix,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: TextFormField(
        controller: controller,
        keyboardType: inputType,
        decoration: InputDecoration(
          labelText: title,
          labelStyle: TextStyle(
            fontSize: 15,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          contentPadding: const EdgeInsets.all(0),
          prefix: prefix,
        ),
        onChanged: (value) {
          setState(() {});
        },
        validator: validator != null
            ? validator
            : (value) {
                if (value.isEmpty) {
                  return "Campo vacío";
                }
                return null;
              },
      ),
    );
  }

  Widget rowInputWidgets(String title1, TextEditingController controller1,
      String title2, TextEditingController controller2, BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 2 - 30,
          child: inputWidget(title1, controller: controller1),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 2 - 30,
          child: inputWidget(title2, controller: controller2),
        ),
      ],
    );
  }

  Widget genderWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text(
              "Sexo:",
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              if (!selectedGender) selectedGender = true;
              setState(() {});
            },
            child: GenderButton(
              gender: true,
              selected: selectedGender,
            ),
          ),
          GestureDetector(
            onTap: () {
              if (selectedGender) selectedGender = false;
              setState(() {});
            },
            child: GenderButton(
              gender: false,
              selected: selectedGender,
            ),
          ),
        ],
      ),
    );
  }
}
