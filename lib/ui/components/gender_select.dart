import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GenderButton extends StatefulWidget {
  bool gender;
  bool selected;
  GenderButton({
    Key key,
    this.gender,
    this.selected,
  }) : super(key: key);

  @override
  _GenderButtonState createState() => _GenderButtonState();
}

class _GenderButtonState extends State<GenderButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color:
            widget.selected == widget.gender ? Colors.blueGrey : Colors.white,
      ),
      child: Image.asset(
        widget.gender ? "assets/male.png" : "assets/female.png",
        color: widget.selected == widget.gender ? Colors.white : Colors.black,
      ),
    );
  }
}
