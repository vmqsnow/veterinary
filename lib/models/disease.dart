import 'package:thalia/db/dbInitHelper.dart';

class Disease {
  int id;
  String name;
  String symptoms;
  DateTime date;
  int petId;

  Disease({
    this.id,
    this.name,
    this.symptoms,
    this.date,
    this.petId,
  });

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "name": this.name,
      "symptoms": this.symptoms,
      "date": this.date.millisecondsSinceEpoch,
      "petId": this.petId,
    };
  }

  Disease.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        symptoms = json['owner'],
        date = DateTime.fromMillisecondsSinceEpoch(json['date']),
        petId = json['petId'];

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "name": DBType.TEXT,
      "symptoms": DBType.TEXT,
      "date": DBType.INTEGER,
      "petId": DBType.INTEGER,
    };
  }
}
