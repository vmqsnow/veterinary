import 'package:thalia/db/dbhelper.dart';

class Pet {
  int id;
  String breed;
  String name;
  String owner;
  bool gender;
  String phoneNumber;
  String fixedphoneNumber;
  DateTime birthDate;
  String image;
  int speciesId;

  Pet({
    this.id,
    this.breed,
    this.name,
    this.owner,
    this.gender,
    this.phoneNumber,
    this.fixedphoneNumber,
    this.birthDate,
    this.speciesId,
  });

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "breed": this.breed,
      "name": this.name,
      "owner": this.owner,
      "gender": this.gender,
      "phoneNumber": this.phoneNumber,
      "fixedphoneNumber": this.fixedphoneNumber,
      "birthDate": this.birthDate.millisecondsSinceEpoch,
      "speciesId": this.speciesId,
    };
  }

  Map<String, dynamic> toMapForDB() {
    return {
      "id": this.id,
      "breed": this.breed,
      "name": this.name,
      "owner": this.owner,
      "gender": this.gender ? 1 : 0,
      "phoneNumber": this.phoneNumber,
      "fixedphoneNumber": this.fixedphoneNumber,
      "birthDate": this.birthDate.millisecondsSinceEpoch,
      "speciesId": this.speciesId,
    };
  }

  Pet.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        breed = json['breed'],
        name = json['name'],
        owner = json['owner'],
        gender = json['gender'] == 1 ? true : false,
        phoneNumber = json['phoneNumber'],
        fixedphoneNumber = json['fixedphoneNumber'],
        birthDate = DateTime.fromMillisecondsSinceEpoch(json['birthDate']),
        speciesId = json['speciesId'];

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "breed": DBType.TEXT,
      "name": DBType.TEXT,
      "owner": DBType.TEXT,
      "gender": DBType.INTEGER,
      "phoneNumber": DBType.TEXT,
      "fixedphoneNumber": DBType.TEXT,
      "birthDate": DBType.INTEGER,
      "speciesId": DBType.INTEGER,
    };
  }
}
