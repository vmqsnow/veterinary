import 'package:thalia/models/pet.dart';

class CacheController {
  bool needToUpdate;
  List<Pet> pets;

  static final CacheController _singleton = CacheController._internal();

  factory CacheController() {
    return _singleton;
  }

  CacheController._internal();
}
