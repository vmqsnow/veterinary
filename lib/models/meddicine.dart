import 'package:thalia/db/dbInitHelper.dart';

class Meddicine {
  int id;
  String name;
  String dose;
  DateTime applicationDate;
  int petId;

  Meddicine({
    this.id,
    this.name,
    this.dose,
    this.applicationDate,
    this.petId,
  });

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "name": this.name,
      "dose": this.dose,
      "applicationDate": applicationDate != null
          ? this.applicationDate.millisecondsSinceEpoch
          : 0,
      "petId": this.petId,
    };
  }

  Meddicine.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        dose = json['dose'],
        applicationDate =
            DateTime.fromMillisecondsSinceEpoch(json['applicationDate']),
        petId = json['petId'];

  static Map<String, String> getFieldsForDB() {
    return {
      "id": DBType.PRIMARY_KEY,
      "name": DBType.TEXT,
      "dose": DBType.TEXT,
      "applicationDate": DBType.INTEGER,
      "petId": DBType.INTEGER,
    };
  }
}
