import 'package:equatable/equatable.dart';

abstract class CalendarState extends Equatable {
  /// notify change state without deep clone state
  final int version;
  
  final List propss;
  CalendarState(this.version,[this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  CalendarState getStateCopy();

  CalendarState getNewVersion();

  @override
  List<Object> get props => ([version, ...propss ?? []]);
}

/// UnInitialized
class UnCalendarState extends CalendarState {

  UnCalendarState(int version) : super(version);

  @override
  String toString() => 'UnCalendarState';

  @override
  UnCalendarState getStateCopy() {
    return UnCalendarState(0);
  }

  @override
  UnCalendarState getNewVersion() {
    return UnCalendarState(version+1);
  }
}

/// Initialized
class InCalendarState extends CalendarState {
  final String hello;

  InCalendarState(int version, this.hello) : super(version, [hello]);

  @override
  String toString() => 'InCalendarState $hello';

  @override
  InCalendarState getStateCopy() {
    return InCalendarState(version, hello);
  }

  @override
  InCalendarState getNewVersion() {
    return InCalendarState(version+1, hello);
  }
}

class ErrorCalendarState extends CalendarState {
  final String errorMessage;

  ErrorCalendarState(int version, this.errorMessage): super(version, [errorMessage]);
  
  @override
  String toString() => 'ErrorCalendarState';

  @override
  ErrorCalendarState getStateCopy() {
    return ErrorCalendarState(version, errorMessage);
  }

  @override
  ErrorCalendarState getNewVersion() {
    return ErrorCalendarState(version+1, 
    errorMessage);
  }
}
