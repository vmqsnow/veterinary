import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scrolling_years_calendar/scrolling_years_calendar.dart';
import 'package:thalia/bloc/calendar/index.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarPage extends StatefulWidget {
  static const String routeName = '/calendar';
  final Function onMenuTap;
  CalendarPage({Key key, @required this.onMenuTap});

  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  // ignore: close_sinks
  final _calendarBloc = CalendarBloc();
  DateTime focusDate;
  bool isInYear;
  Widget yearCalendar;
  Widget monthCalendar;
  Map<DateTime, List> _events;

  @override
  void initState() {
    isInYear = true;
    _events = {
      DateTime(2020, 5, 1): ['Vacuna Juan', 'Desparacitar'],
      DateTime(2020, 5, 15): ['Vacuna Juan', 'Desparacitar'],
      DateTime(2020, 5, 19): ['Vacuna Juan', 'Desparacitar'],
      DateTime(2020, 5, 20): ['Vacuna Juan', 'Desparacitar'],
      DateTime(2020, 1, 20): ['Revisar'],
      DateTime(2020, 2, 20): ['Revisar'],
      DateTime(2020, 3, 20): ['Revisar'],
      DateTime(2020, 4, 20): ['Revisar'],
    };
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: (IconButton(
          icon: Icon(Icons.menu),
          onPressed: widget.onMenuTap,
        )),
        title: Text("Calendar"),
      ),
      body: isInYear
          ? YearCalendar(
              key: ValueKey(1),
              highLighted: _events.keys.toList(),
              onTapMonth: (year, month) {
                setState(() {
                  focusDate = DateTime(year, month, 1);
                  isInYear = false;
                });
              },
            )
          : MonthCalendar(
              key: ValueKey(2),
              focusDate: focusDate,
              events: _events,
              onTapMonth: (_) {
                setState(() {
                  isInYear = true;
                });
              },
            ),
    );
  }
}

class MonthCalendar extends StatefulWidget {
  void Function(DateTime) onTapMonth;
  DateTime focusDate;
  Map<DateTime, List> events;

  MonthCalendar({Key key, this.focusDate, this.onTapMonth, this.events})
      : super(key: key);

  @override
  _MonthCalendarState createState() => _MonthCalendarState();
}

class _MonthCalendarState extends State<MonthCalendar>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  CalendarController _calendarController;
  List selectedEvents;
  bool isBuilding = true;

  @override
  void initState() {
    _calendarController = CalendarController();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    _animationController.forward();

    selectedEvents = widget.events[widget.focusDate] ?? [];
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        isBuilding = false;
      });
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildTableCalendar(),
          Expanded(
            child: ListView.builder(
              itemCount: selectedEvents.length,
              itemBuilder: (context, i) => Container(
                child: Text(
                  selectedEvents[i],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      calendarController: _calendarController,
      events: widget.events,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      onCalendarCreated: (d1, d2, df) {
        _calendarController.setFocusedDay(widget.focusDate);
        _calendarController.setSelectedDay(widget.focusDate);
      },
      daysOfWeekStyle: DaysOfWeekStyle(
        dowTextBuilder: (date, locale) {
          return DateFormat.E(locale).format(date)[0];
        },
        weekdayStyle: TextStyle(fontSize: 12, color: Colors.black87),
        weekendStyle: TextStyle(fontSize: 12, color: Colors.grey),
      ),
      weekendDays: [DateTime.saturday, DateTime.sunday],
      calendarStyle: CalendarStyle(
        selectedColor: Colors.deepOrange[400],
        todayColor: Colors.deepOrange[200],
        markersColor: Colors.brown[700],
        outsideDaysVisible: false,
      ),
      onHeaderTapped: widget.onTapMonth,
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(
          color: Colors.white,
          fontSize: 15.0,
        ),
        centerHeaderTitle: true,
        formatButtonVisible: false,
        formatButtonDecoration: BoxDecoration(
          color: Colors.deepOrange[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onVisibleDaysChanged: (first, last, format) {
        if (!isBuilding) {
          setState(() {
            _calendarController.setFocusedDay(first);
            _calendarController.setSelectedDay(first);
            selectedEvents = widget.events[first] ?? [];
          });
        }
      },
      onDaySelected: (day, list) {
        setState(() {
          selectedEvents = list;
        });
      },
    );
  }
}

class YearCalendar extends StatefulWidget {
  final void Function(int year, int month) onTapMonth;
  final List<DateTime> highLighted;
  YearCalendar({
    Key key,
    this.onTapMonth,
    this.highLighted,
  }) : super(key: key);

  @override
  _YearCalendarState createState() => _YearCalendarState();
}

class _YearCalendarState extends State<YearCalendar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ScrollingYearsCalendar(
        // Required parameters
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now().subtract(const Duration(days: 5 * 365)),
        lastDate: DateTime.now(),
        currentDateColor: Colors.blue,
        // Optional parameters
        highlightedDates: widget.highLighted,
        highlightedDateColor: Colors.deepOrange,
        monthNames: const <String>[
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec',
        ],
        onMonthTap: (int year, int month) => widget.onTapMonth(
          year,
          month,
        ),
        monthTitleStyle: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Colors.blue,
        ),
      ),
    );
  }
}
