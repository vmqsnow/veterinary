import 'dart:async';
import 'dart:developer' as developer;

import 'package:thalia/bloc/calendar/index.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CalendarEvent {
  Stream<CalendarState> applyAsync(
      {CalendarState currentState, CalendarBloc bloc});
  final CalendarRepository _calendarRepository = CalendarRepository();
}

class UnCalendarEvent extends CalendarEvent {
  @override
  Stream<CalendarState> applyAsync({CalendarState currentState, CalendarBloc bloc}) async* {
    yield UnCalendarState(0);
  }
}

class LoadCalendarEvent extends CalendarEvent {
   
  final bool isError;
  @override
  String toString() => 'LoadCalendarEvent';

  LoadCalendarEvent(this.isError);

  @override
  Stream<CalendarState> applyAsync(
      {CalendarState currentState, CalendarBloc bloc}) async* {
    try {
      yield UnCalendarState(0);
      await Future.delayed(Duration(seconds: 1));
      _calendarRepository.test(isError);
      yield InCalendarState(0, 'Hello world');
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadCalendarEvent', error: _, stackTrace: stackTrace);
      yield ErrorCalendarState(0, _?.toString());
    }
  }
}
