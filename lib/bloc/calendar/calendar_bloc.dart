import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:thalia/bloc/calendar/index.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {
  static final CalendarBloc _calendarBlocSingleton = CalendarBloc._internal();
  factory CalendarBloc() {
    return _calendarBlocSingleton;
  }
  CalendarBloc._internal();

  @override
  Future<void> close() async {
    _calendarBlocSingleton.close();
    await super.close();
  }

  @override
  CalendarState get initialState => UnCalendarState(0);

  @override
  Stream<CalendarState> mapEventToState(
    CalendarEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'CalendarBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
