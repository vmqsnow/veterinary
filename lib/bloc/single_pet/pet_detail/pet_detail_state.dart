import 'package:equatable/equatable.dart';
import 'package:thalia/models/disease.dart';
import 'package:thalia/models/meddicine.dart';

abstract class PetDetailState extends Equatable {
  final List propss;
  PetDetailState([this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  PetDetailState getStateCopy();

  @override
  List<Object> get props => ([...propss ?? []]);
}

/// UnInitialized
class UnPetDetailState extends PetDetailState {
  UnPetDetailState() : super();

  @override
  String toString() => 'UnPetDetailState';

  @override
  UnPetDetailState getStateCopy() {
    return UnPetDetailState();
  }
}

/// Initialized
class InPetDetailState extends PetDetailState {
  final List<Disease> diseases;
  final List<Meddicine> meddicines;

  InPetDetailState({this.diseases, this.meddicines})
      : super([diseases, meddicines]);

  @override
  String toString() => 'InPetDetailState';

  @override
  InPetDetailState getStateCopy() {
    return InPetDetailState(diseases: diseases, meddicines: meddicines);
  }
}

class ErrorPetDetailState extends PetDetailState {
  final String errorMessage;

  ErrorPetDetailState(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ErrorPetDetailState';

  @override
  ErrorPetDetailState getStateCopy() {
    return ErrorPetDetailState(errorMessage);
  }
}
