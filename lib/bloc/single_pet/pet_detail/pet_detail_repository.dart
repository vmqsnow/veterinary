import 'package:thalia/bloc/single_pet/pet_detail/index.dart';
import 'package:thalia/db/dbhelper.dart';
import 'package:thalia/models/disease.dart';
import 'package:thalia/models/meddicine.dart';

class PetDetailRepository {
  final PetDetailProvider _petDetailProvider = PetDetailProvider();

  PetDetailRepository();

  Future<List<Meddicine>> getMeddicines(int petId) async {
    try {
      DBHelper dbHelper = DBHelper(
          dbTableName: "meddicine", tableField: Meddicine.getFieldsForDB());
      var result = await dbHelper.findAllFiltered(where: "petId == $petId");
      List<Meddicine> meddicines = result
          .map<Meddicine>(
            (e) => Meddicine.fromJson(e),
          )
          .toList();
      return meddicines;
    } catch (_) {
      return List();
    }
  }

  Future<int> deleteMeddicine(int meddicineId) async {
    try {
      DBHelper dbHelper = DBHelper(
          dbTableName: "meddicine", tableField: Meddicine.getFieldsForDB());
      var response = await dbHelper.delete(meddicineId);
      return response;
    } catch (_) {
      return 0;
    }
  }

  Future<List<Meddicine>> deleteMeddicines(
      List<Meddicine> meddicinesForDelete, int petId) async {
    try {
      await Future.forEach<Meddicine>(
        meddicinesForDelete,
        (element) async {
          await deleteMeddicine(element.id);
        },
      );
      return await getMeddicines(petId);
    } catch (_) {
      return List();
    }
  }

  Future<List<Disease>> getDiseases(int petId) async {
    try {
      DBHelper dbHelper = DBHelper(
          dbTableName: "disease", tableField: Disease.getFieldsForDB());
      var result = await dbHelper.findAllFiltered(where: "petId == $petId");
      List<Disease> diseases = result
          .map<Disease>(
            (e) => Disease.fromJson(e),
          )
          .toList();
      return diseases;
    } catch (_) {
      return List();
    }
  }

  Future<bool> addMeddicine(Meddicine meddicine) async {
    try {
      DBHelper dbHelper = DBHelper(
          dbTableName: "meddicine", tableField: Meddicine.getFieldsForDB());
      await dbHelper.saveOne(meddicine.toMap());
      return true;
    } catch (_) {
      return false;
    }
  }

  Future<bool> addDisease(Disease disease) async {
    try {
      DBHelper dbHelper = DBHelper(
          dbTableName: "disease", tableField: Meddicine.getFieldsForDB());
      await dbHelper.saveOne(disease.toMap());
      return true;
    } catch (_) {
      return false;
    }
  }
}
