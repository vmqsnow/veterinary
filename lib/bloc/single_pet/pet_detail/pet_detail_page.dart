import 'package:flutter/material.dart';
import 'package:thalia/bloc/single_pet/pet_detail/index.dart';
import 'package:thalia/models/pet.dart';

class PetDetailPage extends StatefulWidget {
  static const String routeName = '/petDetail';
  Pet pet;

  PetDetailPage({this.pet});

  @override
  _PetDetailPageState createState() => _PetDetailPageState();
}

class _PetDetailPageState extends State<PetDetailPage> {
  final _petDetailBloc = PetDetailBloc();

  @override
  Widget build(BuildContext context) {
    _petDetailBloc.pet = widget.pet;
    return PetDetailScreen(petDetailBloc: _petDetailBloc);
  }
}
