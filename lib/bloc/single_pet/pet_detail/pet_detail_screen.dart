import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:thalia/bloc/single_pet/pet_detail/index.dart';
import 'package:intl/intl.dart';
import 'package:thalia/models/disease.dart';
import 'package:thalia/models/meddicine.dart';
import 'package:thalia/models/pet.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class PetDetailScreen extends StatefulWidget {
  const PetDetailScreen({
    Key key,
    @required PetDetailBloc petDetailBloc,
  })  : _petDetailBloc = petDetailBloc,
        super(key: key);

  final PetDetailBloc _petDetailBloc;

  @override
  PetDetailScreenState createState() {
    return PetDetailScreenState();
  }
}

class PetDetailScreenState extends State<PetDetailScreen> {
  PetDetailScreenState();

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PetDetailBloc, PetDetailState>(
        bloc: widget._petDetailBloc,
        builder: (
          BuildContext context,
          PetDetailState currentState,
        ) {
          if (currentState is UnPetDetailState) {
            return Material(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(
                      backgroundColor: Colors.transparent,
                      valueColor: AlwaysStoppedAnimation(
                        Color(0x426e6f).withOpacity(1),
                      ),
                    ),
                    Text("Cargando datos...")
                  ],
                ),
              ),
            );
          }
          if (currentState is ErrorPetDetailState) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(currentState.errorMessage ?? 'Error'),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: RaisedButton(
                      color: Colors.blue,
                      child: Text('reload'),
                      onPressed: _load,
                    ),
                  ),
                ],
              ),
            );
          }
          if (currentState is InPetDetailState) {
            return PetDetailsView(
              pet: widget._petDetailBloc.pet,
              meddicines: currentState.meddicines,
              diseases: currentState.diseases,
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void _load([bool isError = false]) {
    widget._petDetailBloc.add(UnPetDetailEvent());
  }
}

class PetDetailsView extends StatefulWidget {
  Pet pet;
  List<Meddicine> meddicines;
  List<Disease> diseases;

  PetDetailsView({
    Key key,
    this.pet,
    this.diseases,
    this.meddicines,
  }) : super(key: key);

  @override
  _PetDetailsViewState createState() => _PetDetailsViewState();
}

class _PetDetailsViewState extends State<PetDetailsView>
    with SingleTickerProviderStateMixin {
  TabController controller;
  bool editingMode = false;

  @override
  void initState() {
    controller = TabController(
      vsync: this,
      initialIndex: 0,
      length: 2,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              if (controller.index == 0) {
                return AddMedicine(pet: widget.pet);
              } else {
                return AddDesease(pet: widget.pet);
              }
            },
          );
        },
        splashColor: Colors.red,
        backgroundColor: Color(0x426e6f).withOpacity(1),
        child: Center(
          child: Image.asset(
            controller.index == 0
                ? "assets/pills64.png"
                : "assets/dog-cone2-64.png",
            height: 25,
            color: Colors.white,
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color(0x426e6f).withOpacity(1),
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(
                  left: 15,
                  right: 15,
                  bottom: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                      ),
                      backgroundColor: Colors.white,
                      builder: (context) {
                        return PetDetailSheet(pet: widget.pet);
                      });
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.grey,
                            child: widget.pet.image == "" ||
                                    widget.pet.image == null
                                ? Text(
                                    widget.pet.name[0],
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                    ),
                                  )
                                : Container(),
                          ),
                          Positioned(
                            left: 0,
                            bottom: 0,
                            child: Image.asset(
                              widget.pet.speciesId == 1
                                  ? "assets/dog64.png"
                                  : "assets/cat64.png",
                              height: 25,
                            ),
                          )
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.pet.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            widget.pet.breed,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            widget.pet.gender
                                ? "assets/male.svg"
                                : "assets/female.svg",
                            color: Colors.white,
                            height: 20,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 40,
                child: TabBar(
                  controller: controller,
                  indicatorColor: Colors.transparent,
                  tabs: <Widget>[
                    Container(
                      height: 40,
                      child: Center(
                        child: Text(
                          "Vacunas",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: controller.index == 0
                                ? FontWeight.w700
                                : FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      child: Center(
                        child: Text(
                          "Enfermedades",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: controller.index == 1
                                ? FontWeight.w700
                                : FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: TabBarView(
                          controller: controller,
                          children: <Widget>[
                            MeddicinesList(
                              key: Key(widget.meddicines.hashCode.toString()),
                              meddicines: widget.meddicines,
                            ),
                            DiseasesList(
                              key: Key(widget.diseases.hashCode.toString()),
                              diseases: widget.diseases,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PetDetailSheet extends StatelessWidget {
  Pet pet;
  PetDetailSheet({
    Key key,
    this.pet,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .9,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(top: 20),
      child: Stack(
        children: <Widget>[
          Positioned(
            right: 10,
            top: 0,
            child: Opacity(
              opacity: 0.05,
              child: Image.asset(
                pet.speciesId == 0 ? "assets/cat64.png" : "assets/dog64.png",
                scale: 0.7,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.grey.shade300,
                      radius: 60,
                      child: Text(
                        pet.name[0].toUpperCase(),
                        style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 10,
                      bottom: 0,
                      child: Container(
                        height: 30,
                        padding: const EdgeInsets.all(6),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.grey,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(0, 1),
                              )
                            ]),
                        child: Image.asset(
                          pet.gender ? "assets/male.png" : "assets/female.png",
                        ),
                      ),
                    ),
                  ],
                ),
                if (pet.phoneNumber != null && pet.phoneNumber.isNotEmpty)
                  Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              UrlLauncher.launch("tel:${pet.phoneNumber}");
                            },
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 30,
                                  padding: const EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blueAccent,
                                  ),
                                  child: Icon(
                                    Icons.call,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                ),
                                Text(
                                  "Llamar",
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.blueAccent,
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              UrlLauncher.launch("tel:*99${pet.phoneNumber}");
                            },
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 30,
                                  padding: const EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blueAccent,
                                  ),
                                  child: Icon(
                                    Icons.call,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                ),
                                Text(
                                  "*99",
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.blueAccent,
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              UrlLauncher.launch(
                                  "sms:${pet.phoneNumber}?body=Hola%20soy%20la%20veterinaria");
                            },
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 30,
                                  padding: const EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blueAccent,
                                  ),
                                  child: Icon(
                                    Icons.message,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                ),
                                Text(
                                  "Mensaje",
                                  style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.blueAccent,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      )),
                Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  child: Column(
                    children: <Widget>[
                      titleWidget("Nombre:", pet.name, context),
                      titleWidget("Raza:", pet.breed, context),
                      titleWidget(
                          "Edad:",
                          "${(DateTime.now().difference(pet.birthDate).inDays / 365).toStringAsFixed(1)}",
                          context),
                      titleWidget("Dueño:", pet.owner, context),
                      titleWidget("Teléfono:", pet.phoneNumber, context),
                      titleWidget("Fijo:", pet.fixedphoneNumber, context),
                      titleWidget("Último medicamento:", "TODO", context),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget titleWidget(String title, String value, BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text(
              title,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Text(value),
        ],
      ),
    );
  }
}

class AddMedicine extends StatefulWidget {
  Pet pet;
  Meddicine meddicine;
  AddMedicine({
    Key key,
    this.pet,
    this.meddicine,
  })  : assert(pet == null || meddicine == null,
            "Cannot provide both pet and meddicine"),
        super(key: key);

  @override
  _AddMedicineState createState() => _AddMedicineState();
}

class _AddMedicineState extends State<AddMedicine> {
  TextEditingController meddicineController;
  TextEditingController doseController;
  DateTime dateController;
  bool loading, editingMode;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    loading = false;
    editingMode = true;
    meddicineController = TextEditingController();
    doseController = TextEditingController();

    if (widget.meddicine != null) {
      editingMode = false;
      meddicineController.text = widget.meddicine.name;
      doseController.text = widget.meddicine.dose;
      dateController = widget.meddicine.applicationDate;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Row(
        children: <Widget>[
          Text(
            (widget.meddicine == null
                    ? "Adicionar"
                    : editingMode ? "Editar" : "Ver") +
                " medicamento",
            style: TextStyle(
              fontSize: 17,
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w600,
            ),
          ),
          if (widget.meddicine != null && !editingMode)
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: InkWell(
                onTap: () {
                  setState(() {
                    editingMode = true;
                  });
                },
                child: Icon(
                  Icons.edit,
                  color: Theme.of(context).primaryColor,
                  size: 15,
                ),
              ),
            )
        ],
      ),
      content: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: TextFormField(
                        controller: meddicineController,
                        enabled: editingMode,
                        decoration: InputDecoration(
                          labelText: "Medicamento",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          hintText: "Medicamento",
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: TextFormField(
                        controller: doseController,
                        enabled: editingMode,
                        decoration: InputDecoration(
                          labelText: "Dosis",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          hintText: "150 cc/ml",
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: DateTimeField(
                        initialValue: dateController,
                        enabled: editingMode,
                        decoration: InputDecoration(
                          labelText: "Fecha",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        format: DateFormat.yMMMEd(),
                        onShowPicker: (context, currentValue) async {
                          final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100),
                          );
                          if (date != null) {
                            return date;
                          } else {
                            return currentValue;
                          }
                        },
                        onChanged: (date) {
                          dateController = date;
                          setState(() {});
                        },
                        validator: (value) {
                          if (value == null) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      actionsPadding: const EdgeInsets.symmetric(horizontal: 18),
      actions: <Widget>[
        if (editingMode)
          RaisedButton(
            onPressed: !loading ? () => Navigator.of(context).pop() : null,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Text("Cancelar"),
          ),
        RaisedButton(
          onPressed: () async {
            if (editingMode) {
              if (!loading && formKey.currentState.validate()) {
                setState(() {
                  loading = true;
                });
                PetDetailRepository repository = PetDetailRepository();

                Meddicine meddicine = Meddicine(
                  name: meddicineController.value.text,
                  dose: doseController.value.text,
                  petId: widget.meddicine == null
                      ? widget.pet.id
                      : widget.meddicine.petId,
                  applicationDate: dateController,
                  id: widget.meddicine == null
                      ? DateTime.now().millisecondsSinceEpoch
                      : widget.meddicine.id,
                );

                var result = await repository.addMeddicine(meddicine);

                if (result) {
                  var meddicines =
                      await repository.getMeddicines(meddicine.petId);
                  PetDetailBloc bloc = PetDetailBloc();
                  var currentState = bloc.state;

                  Navigator.of(context).pop();
                  bloc.add(
                    FastLoadDetailEvent(
                      meddicines: meddicines,
                      diseases: (currentState as InPetDetailState).diseases,
                    ),
                  );
                } else {
                  setState(() {
                    loading = false;
                  });
                }
              }
            } else {
              Navigator.of(context).pop();
            }
          },
          color: Theme.of(context).primaryColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: loading
              ? Container(
                  height: 30,
                  child: Transform.scale(
                    scale: 0.7,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(
                        Colors.white,
                      ),
                    ),
                  ),
                )
              : Text(editingMode ? "Adicionar" : "Aceptar"),
        )
      ],
    );
  }
}

class AddDesease extends StatefulWidget {
  Pet pet;
  AddDesease({Key key, this.pet}) : super(key: key);

  @override
  _AddDeseaseState createState() => _AddDeseaseState();
}

class _AddDeseaseState extends State<AddDesease> {
  TextEditingController diseaseController;
  TextEditingController symptomsController;
  DateTime dateController;
  bool loading;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    loading = false;
    symptomsController = TextEditingController();
    diseaseController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Text(
        "Adicionar enfermedad",
        style: TextStyle(
          fontSize: 17,
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.w600,
        ),
      ),
      content: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: TextFormField(
                        controller: diseaseController,
                        decoration: InputDecoration(
                          labelText: "Nombre de enfermedad",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: DateTimeField(
                        initialValue: dateController,
                        decoration: InputDecoration(
                          labelText: "Fecha",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        format: DateFormat.yMMMEd(),
                        onShowPicker: (context, currentValue) async {
                          final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100),
                          );
                          if (date != null) {
                            return date;
                          } else {
                            return currentValue;
                          }
                        },
                        onChanged: (date) {
                          setState(() {
                            dateController = date;
                          });
                        },
                        validator: (value) {
                          if (value == null) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: TextFormField(
                        maxLines: 3,
                        minLines: 1,
                        controller: symptomsController,
                        decoration: InputDecoration(
                          labelText: "Síntomas",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          contentPadding: const EdgeInsets.all(0),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Campo vacío";
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      actionsPadding: const EdgeInsets.symmetric(horizontal: 18),
      actions: <Widget>[
        RaisedButton(
          onPressed: !loading ? () => Navigator.of(context).pop() : null,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Text("Cancelar"),
        ),
        RaisedButton(
          onPressed: () async {
            //Disease
            if (!loading && formKey.currentState.validate()) {
              setState(() {
                loading = true;
              });
              PetDetailRepository repository = PetDetailRepository();

              Disease disease = Disease(
                name: diseaseController.value.text,
                symptoms: symptomsController.value.text,
                petId: widget.pet.id,
                date: dateController,
                id: DateTime.now().millisecondsSinceEpoch,
              );

              var result = await repository.addDisease(disease);

              if (result) {
                var diseases = await repository.getDiseases(disease.petId);
                PetDetailBloc bloc = PetDetailBloc();
                var currentState = bloc.state;

                Navigator.of(context).pop();
                bloc.add(
                  FastLoadDetailEvent(
                    diseases: diseases,
                    meddicines: (currentState as InPetDetailState).meddicines,
                  ),
                );
              } else {
                setState(() {
                  loading = false;
                });
              }
            }
          },
          color: Theme.of(context).primaryColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: loading
              ? Container(
                  height: 30,
                  child: Transform.scale(
                    scale: 0.7,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(
                        Colors.white,
                      ),
                    ),
                  ),
                )
              : Text("Adicionar"),
        )
      ],
    );
  }
}

class MeddicinesList extends StatefulWidget {
  final List<Meddicine> meddicines;
  MeddicinesList({Key key, @required this.meddicines}) : super(key: key);

  @override
  _MeddicinesListState createState() => _MeddicinesListState();
}

class _MeddicinesListState extends State<MeddicinesList> {
  bool editingMode = false;
  List<Map<String, dynamic>> selectionItems;

  @override
  void initState() {
    selectionItems = widget.meddicines
        .map<Map<String, dynamic>>((e) => {
              "selected": false,
              "meddicine": e,
            })
        .toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Map> selectedItems =
        selectionItems.where((element) => element["selected"]).toList();
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: selectionItems.length == 0
          ? Center(
              child: Text(
                "No se le han puesto vacunas",
                style: TextStyle(fontSize: 15),
              ),
            )
          : Column(
              children: <Widget>[
                if (editingMode)
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 30,
                    margin: const EdgeInsets.only(top: 5, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          onPressed: selectedItems.length > 0
                              ? () async {
                                  List<Meddicine> meddicinesForDelete =
                                      selectedItems
                                          .map<Meddicine>((e) => e["meddicine"])
                                          .toList();
                                  var response = await showDialog<bool>(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text(
                                            "Alerta",
                                            style: TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                          content: Container(
                                            child: Text(
                                              "Desea eliminar ${selectedItems.length} elementos",
                                              style: TextStyle(
                                                fontSize: 14,
                                              ),
                                            ),
                                          ),
                                          actions: <Widget>[
                                            RaisedButton(
                                              onPressed: () {
                                                return Navigator.of(context)
                                                    .pop(false);
                                              },
                                              child: Text("Cancelar"),
                                            ),
                                            RaisedButton(
                                              onPressed: () {
                                                return Navigator.of(context)
                                                    .pop(true);
                                              },
                                              child: Text("Aceptar"),
                                            )
                                          ],
                                        );
                                      });
                                  if (response) {
                                    PetDetailRepository repository =
                                        PetDetailRepository();

                                    var meddicines =
                                        await repository.deleteMeddicines(
                                            meddicinesForDelete,
                                            meddicinesForDelete[0].petId);

                                    PetDetailBloc bloc = PetDetailBloc();
                                    var currentState = bloc.state;

                                    bloc.add(
                                      FastLoadDetailEvent(
                                        meddicines: meddicines,
                                        diseases:
                                            (currentState as InPetDetailState)
                                                .diseases,
                                      ),
                                    );
                                  }
                                }
                              : null,
                          color: Colors.grey.shade100,
                          child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.delete,
                                color: Theme.of(context).primaryColor,
                              ),
                              Text(
                                "Eliminar ${selectedItems.length} elementos",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        FlatButton(
                          onPressed: () {
                            selectionItems.forEach((element) {
                              element["selected"] = false;
                            });
                            setState(() {
                              editingMode = !editingMode;
                            });
                          },
                          color: Colors.grey.shade100,
                          child: Text("Cancelar",
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                              )),
                        )
                      ],
                    ),
                  ),
                Expanded(
                  child: ListView.builder(
                    itemCount: selectionItems.length,
                    itemBuilder: (context, index) {
                      bool selected = selectionItems[index]["selected"];
                      Meddicine meddicine = selectionItems[index]["meddicine"];
                      return GestureDetector(
                        onTap: () {
                          if (!editingMode) {
                            showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AddMedicine(
                                  meddicine: meddicine,
                                );
                              },
                            );
                          }
                        },
                        onLongPress: () {
                          if (!editingMode) {
                            setState(() {
                              selectionItems[index]["selected"] = true;
                              editingMode = !editingMode;
                            });
                          }
                        },
                        child: Container(
                          margin: const EdgeInsets.symmetric(vertical: 2),
                          color: selected ? Colors.blue.withOpacity(0.3) : null,
                          child: !editingMode
                              ? ListTile(
                                  contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 5,
                                    vertical: 0,
                                  ),
                                  leading: Image.asset(
                                    index % 3 == 1
                                        ? "assets/srynge64.png"
                                        : index % 3 == 2
                                            ? "assets/dose64.png"
                                            : "assets/pills64.png",
                                    height: 30,
                                    color: Color(0x426e6f).withOpacity(1),
                                  ),
                                  title: Text(
                                    meddicine.name,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  subtitle: Text(
                                    meddicine.dose,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  trailing: Text(
                                    "${DateFormat("dd/MM/yy").format(meddicine.applicationDate)}",
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0x426e6f).withOpacity(1),
                                    ),
                                  ),
                                )
                              : Container(
                                  height: 60,
                                  child: Row(
                                    children: <Widget>[
                                      Checkbox(
                                        onChanged: (value) {
                                          setState(() {
                                            selectionItems[index]["selected"] =
                                                value;
                                          });
                                        },
                                        value: selected,
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 5, vertical: 0),
                                          leading: Image.asset(
                                            index % 3 == 1
                                                ? "assets/srynge64.png"
                                                : index % 3 == 2
                                                    ? "assets/dose64.png"
                                                    : "assets/pills64.png",
                                            height: 30,
                                            color:
                                                Color(0x426e6f).withOpacity(1),
                                          ),
                                          title: Text(
                                            meddicine.name,
                                            style: TextStyle(fontSize: 13),
                                          ),
                                          subtitle: Text(
                                            meddicine.dose,
                                            style: TextStyle(fontSize: 12),
                                          ),
                                          trailing: Text(
                                            "${DateFormat("dd/MM/yy").format(meddicine.applicationDate)}",
                                            style: TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0x426e6f)
                                                  .withOpacity(1),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
    );
  }
}

class DiseasesList extends StatefulWidget {
  List<Disease> diseases;
  DiseasesList({Key key, @required this.diseases}) : super(key: key);

  @override
  _DiseasesListState createState() => _DiseasesListState();
}

class _DiseasesListState extends State<DiseasesList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.diseases.length == 0
          ? Center(
              child: Text(
                "No se le han puesto enfermedades",
                style: TextStyle(fontSize: 15),
              ),
            )
          : ListView.builder(
              itemCount: widget.diseases.length,
              itemBuilder: (context, index) {
                Disease disease = widget.diseases[index];
                return GestureDetector(
                  onTap: () {},
                  child: Container(
                    child: ListTile(
                      leading: Image.asset(
                        "assets/dog-cone2-64.png",
                        height: 30,
                        color: Color(0x426e6f).withOpacity(1),
                      ),
                      title: Text(
                        disease.name,
                        style: TextStyle(fontSize: 13),
                      ),
                      trailing: Text(
                        "${DateFormat("dd/MM/yy").format(disease.date)}",
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          color: Color(0x426e6f).withOpacity(1),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
    );
  }
}
