import 'dart:async';

import 'package:thalia/bloc/single_pet/pet_detail/index.dart';
import 'package:meta/meta.dart';
import 'package:thalia/models/disease.dart';
import 'package:thalia/models/meddicine.dart';

@immutable
abstract class PetDetailEvent {
  Stream<PetDetailState> applyAsync(
      {PetDetailState currentState, PetDetailBloc bloc});
  final PetDetailRepository _petDetailRepository = PetDetailRepository();
}

class UnPetDetailEvent extends PetDetailEvent {
  @override
  Stream<PetDetailState> applyAsync(
      {PetDetailState currentState, PetDetailBloc bloc}) async* {
    yield UnPetDetailState();

    List<Disease> diseases =
        await _petDetailRepository.getDiseases(bloc.pet.id);

    List<Meddicine> meddicines =
        await _petDetailRepository.getMeddicines(bloc.pet.id);

    yield InPetDetailState(
      meddicines: meddicines,
      diseases: diseases,
    );
  }
}

class FastLoadDetailEvent extends PetDetailEvent {
  List<Disease> diseases;
  List<Meddicine> meddicines;

  FastLoadDetailEvent({this.diseases, this.meddicines});

  @override
  Stream<PetDetailState> applyAsync(
      {PetDetailState currentState, PetDetailBloc bloc}) async* {
    yield InPetDetailState(
      meddicines: meddicines,
      diseases: diseases,
    );
  }
}
