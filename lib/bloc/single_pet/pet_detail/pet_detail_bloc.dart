import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:thalia/bloc/single_pet/pet_detail/index.dart';
import 'package:thalia/models/pet.dart';

class PetDetailBloc extends Bloc<PetDetailEvent, PetDetailState> {
  // todo: check singleton for logic in project
  static final PetDetailBloc _petDetailBlocSingleton =
      PetDetailBloc._internal();
  Pet pet;
  factory PetDetailBloc() {
    return _petDetailBlocSingleton;
  }
  PetDetailBloc._internal();

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  @override
  PetDetailState get initialState => UnPetDetailState();

  @override
  Stream<PetDetailState> mapEventToState(
    PetDetailEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'PetDetailBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
