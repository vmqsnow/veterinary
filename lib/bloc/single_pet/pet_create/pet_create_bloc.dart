import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';

class PetCreateBloc extends Bloc<PetCreateEvent, PetCreateState> {
  // todo: check singleton for logic in project
  static final PetCreateBloc _petCreateBlocSingleton =
      PetCreateBloc._internal();
  factory PetCreateBloc() {
    return _petCreateBlocSingleton;
  }
  PetCreateBloc._internal();

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  @override
  PetCreateState get initialState => UnPetCreateState();

  @override
  Stream<PetCreateState> mapEventToState(
    PetCreateEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'PetCreateBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
