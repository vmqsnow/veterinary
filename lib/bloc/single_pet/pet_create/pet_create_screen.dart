import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';
import 'package:thalia/ui/create_pet.dart';

class PetCreateScreen extends StatefulWidget {
  const PetCreateScreen({
    Key key,
    @required PetCreateBloc petCreateBloc,
  })  : _petCreateBloc = petCreateBloc,
        super(key: key);

  final PetCreateBloc _petCreateBloc;

  @override
  PetCreateScreenState createState() {
    return PetCreateScreenState();
  }
}

class PetCreateScreenState extends State<PetCreateScreen> {
  PetCreateScreenState();

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PetCreateBloc, PetCreateState>(
      bloc: widget._petCreateBloc,
      builder: (
        BuildContext context,
        PetCreateState currentState,
      ) {
        if (currentState is UnPetCreateState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (currentState is ErrorPetCreateState) {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(currentState.errorMessage ?? 'Error'),
            ],
          ));
        }
        if (currentState is InPetCreateState) {
          return CreatePetSheet();
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  void _load([bool isError = false]) {
    widget._petCreateBloc.add(LoadPetCreateEvent());
  }
}
