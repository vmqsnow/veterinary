import 'dart:async';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thalia/bloc/home/home_bloc.dart';
import 'package:thalia/bloc/home/home_event.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';
import 'package:meta/meta.dart';
import 'package:thalia/models/pet.dart';

@immutable
abstract class PetCreateEvent {
  Stream<PetCreateState> applyAsync(
      {PetCreateState currentState, PetCreateBloc bloc});
  final PetCreateRepository _petCreateRepository = PetCreateRepository();
}

class UnPetCreateEvent extends PetCreateEvent {
  @override
  Stream<PetCreateState> applyAsync(
      {PetCreateState currentState, PetCreateBloc bloc}) async* {
    yield UnPetCreateState();
  }
}

class LoadPetCreateEvent extends PetCreateEvent {
  @override
  String toString() => 'LoadPetCreateEvent';

  LoadPetCreateEvent();

  @override
  Stream<PetCreateState> applyAsync(
      {PetCreateState currentState, PetCreateBloc bloc}) async* {
    try {
      yield InPetCreateState();
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadPetCreateEvent', error: _, stackTrace: stackTrace);
      yield ErrorPetCreateState(_?.toString());
    }
  }
}

class StorePetEvent extends PetCreateEvent {
  Pet pet;
  BuildContext context;
  @override
  String toString() => 'StorePetEvent';

  StorePetEvent({
    @required this.pet,
    @required this.context,
  });

  @override
  Stream<PetCreateState> applyAsync(
      {PetCreateState currentState, PetCreateBloc bloc}) async* {
    try {
      bool result = await _petCreateRepository.addPet(pet);
      if (result) {
        Navigator.of(context).pop();
        HomeBloc().add(ReloadHomeEvent());
        await showMessage(
            "Información", "Se guardaron los datos correctamente");
        yield UnPetCreateState();
      } else {
        await showMessage("Error", "No se guardaron los datos");
        yield (currentState as InPetCreateState).copyWith(false);
      }
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadPetCreateEvent', error: _, stackTrace: stackTrace);
      yield ErrorPetCreateState(_?.toString());
    }
  }

  Future<T> showMessage<T>(String title, String message) async {
    return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          actions: <Widget>[
            RaisedButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("Aceptar"),
            )
          ],
          content: Text(message),
          title: Text(title),
        );
      },
    );
  }
}
