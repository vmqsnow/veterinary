import 'package:flutter/material.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';

class PetCreatePage extends StatefulWidget {
  static const String routeName = '/petCreate';

  @override
  _PetCreatePageState createState() => _PetCreatePageState();
}

class _PetCreatePageState extends State<PetCreatePage> {
  final _petCreateBloc = PetCreateBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PetCreate'),
      ),
      body: PetCreateScreen(petCreateBloc: _petCreateBloc),
    );
  }
}
