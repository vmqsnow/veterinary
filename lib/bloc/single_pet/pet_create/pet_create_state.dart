import 'package:equatable/equatable.dart';

abstract class PetCreateState extends Equatable {
  final List propss;
  PetCreateState([this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  PetCreateState getStateCopy();

  PetCreateState getNewVersion();

  @override
  List<Object> get props => ([propss ?? []]);
}

/// UnInitialized
class UnPetCreateState extends PetCreateState {
  UnPetCreateState();

  @override
  String toString() => 'UnPetCreateState';

  @override
  UnPetCreateState getStateCopy() {
    return UnPetCreateState();
  }

  @override
  UnPetCreateState getNewVersion() {
    return UnPetCreateState();
  }
}

/// Initialized
class InPetCreateState extends PetCreateState {
  bool loading;
  InPetCreateState({this.loading}) : super([loading]);

  @override
  String toString() => 'InPetCreateState';

  @override
  InPetCreateState getStateCopy() {
    return InPetCreateState();
  }

  @override
  InPetCreateState getNewVersion() {
    return InPetCreateState();
  }

  @override
  InPetCreateState copyWith(bool loading) {
    return InPetCreateState(loading: loading);
  }
}

/// Loading
class LoadingPetCreateState extends PetCreateState {
  LoadingPetCreateState() : super([]);

  @override
  String toString() => 'InPetCreateState';

  @override
  LoadingPetCreateState getStateCopy() {
    return LoadingPetCreateState();
  }

  @override
  LoadingPetCreateState getNewVersion() {
    return LoadingPetCreateState();
  }
}

class ErrorPetCreateState extends PetCreateState {
  final String errorMessage;

  ErrorPetCreateState(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ErrorPetCreateState';

  @override
  ErrorPetCreateState getStateCopy() {
    return ErrorPetCreateState(errorMessage);
  }

  @override
  ErrorPetCreateState getNewVersion() {
    return ErrorPetCreateState(errorMessage);
  }
}
