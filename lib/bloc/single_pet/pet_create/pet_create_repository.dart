import 'package:thalia/bloc/single_pet/pet_create/index.dart';
import 'package:thalia/db/dbhelper.dart';
import 'package:thalia/models/pet.dart';

class PetCreateRepository {
  final PetCreateProvider _petCreateProvider = PetCreateProvider();

  PetCreateRepository();

  Future<bool> addPet(Pet pet) async {
    try {
      DBHelper dbHelper =
          DBHelper(dbTableName: "pets", tableField: Pet.getFieldsForDB());
      await dbHelper.saveOne(pet.toMap());
      return true;
    } catch (_) {
      return false;
    }
  }
}
