import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thalia/bloc/calendar/index.dart';
import 'package:thalia/bloc/single_pet/pet_create/index.dart';
import 'package:thalia/bloc/single_pet/pet_detail/pet_detail_page.dart';
import 'package:thalia/models/pet.dart';
import 'index.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key key,
    @required HomeBloc homeBloc,
  })  : _homeBloc = homeBloc,
        super(key: key);

  final HomeBloc _homeBloc;

  @override
  HomeScreenState createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  HomeScreenState();

  @override
  void initState() {
    widget._homeBloc.add(UnHomeEvent());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
        bloc: widget._homeBloc,
        builder: (
          BuildContext context,
          HomeState state,
        ) {
          if (state is UnHomeState) {
            return CircularProgressIndicator();
          }
          if (state is InHomeState) {
            return HomeView(
              pets: state.pets,
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}

class HomeView extends StatefulWidget {
  final List<Pet> pets;
  HomeView({Key key, this.pets}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  bool isMinimized = true;
  List<Widget> pagesOrder;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
      lowerBound: 0.7,
      upperBound: 1,
    )..addListener(() {
        setState(() {});
      });
    pagesOrder = [
      CalendarPage(
        onMenuTap: () => animationController.reverse(),
      ),
      PetsView(
        pets: widget.pets,
        onMenuTap: () => animationController.reverse(),
      ),
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(children: <Widget>[
          MainMenu(
            onTapPets: () {
              animationController.forward();
              setState(() {
                pagesOrder = [
                  PetsView(
                    pets: widget.pets,
                    onMenuTap: () => animationController.reverse(),
                  ),
                  CalendarPage(
                    onMenuTap: () => animationController.reverse(),
                  ),
                ];
              });
            },
            onTapCalendar: () {
              animationController.forward();
              setState(() {
                pagesOrder = [
                  CalendarPage(
                    onMenuTap: () => animationController.reverse(),
                  ),
                  PetsView(
                    pets: widget.pets,
                    onMenuTap: () => animationController.reverse(),
                  ),
                ];
              });
            },
          ),
          ...getPages(pagesOrder)
        ]),
      ),
    );
  }

  List<Widget> getPages(List<Widget> pagesOrder) {
    var slideHorizontalAmount =
        (1 - animationController.value) * MediaQuery.of(context).size.width * 2;
    var scaleValue = animationController.value;
    var cornerRadius = 60 * (1 - animationController.value);

    var staticSlideHorizontalAmount = MediaQuery.of(context).size.width * .6;
    double staticCornerRadius = 18;

    List<Widget> response = List();
    for (var i = pagesOrder.length - 1; i >= 0; i--) {
      if (i == 0) {
        response.add(
          Opacity(
            opacity: 1,
            child: Transform(
              transform: Matrix4.translationValues(slideHorizontalAmount, 0, 0)
                ..scale(
                  scaleValue,
                  scaleValue,
                ),
              alignment: Alignment.centerLeft,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      blurRadius: 10.0,
                      spreadRadius: 10.0,
                      color: Colors.grey.withOpacity(0.2),
                      offset: Offset(1, 1),
                    )
                  ],
                  borderRadius: BorderRadius.circular(cornerRadius),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(cornerRadius),
                  child: Container(
                    color: Colors.white,
                    child: AbsorbPointer(
                      absorbing: animationController.value != 1,
                      child: pagesOrder[i],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      } else {
        response.add(
          Opacity(
            opacity: 0.7,
            child: Transform(
              transform: Matrix4.translationValues(
                  staticSlideHorizontalAmount - (i * 20), 0, 0)
                ..scale(
                  0.63,
                  0.63,
                ),
              alignment: Alignment.centerLeft,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(staticCornerRadius),
                child: Container(
                  color: Colors.white,
                  child: AbsorbPointer(
                    absorbing: true,
                    child: pagesOrder[i],
                  ),
                ),
              ),
            ),
          ),
        );
      }
    }
    return response;
  }
}

class MainMenu extends StatelessWidget {
  final Function onTapPets;
  final Function onTapCalendar;
  const MainMenu({
    Key key,
    this.onTapPets,
    this.onTapCalendar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
      color: Color(0x426e6f).withOpacity(1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                radius: 20,
                backgroundColor: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  "Thalia Rodríguez Díaz",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
          Container(
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: onTapPets,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 15,
                    ),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.pets,
                          color: Colors.white,
                          size: 17,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("Mascotas",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: onTapCalendar,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.calendar_today,
                          color: Colors.white,
                          size: 17,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("Calendario",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                      ),
                      backgroundColor: Colors.white,
                      builder: (context) {
                        return PetCreateScreen(
                          petCreateBloc: PetCreateBloc(),
                        );
                      },
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 15,
                    ),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.add_box,
                          color: Colors.white,
                          size: 17,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Adicionar mascota",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 15,
                  ),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add_box,
                        color: Colors.white,
                        size: 17,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          "Adicionar especie",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.settings,
                color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text("Opciones",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class PetsView extends StatefulWidget {
  final List<Pet> pets;
  final Function onMenuTap;

  PetsView({
    Key key,
    this.onMenuTap,
    @required this.pets,
  }) : super(key: key);

  @override
  _PetsViewState createState() => _PetsViewState();
}

class _PetsViewState extends State<PetsView> {
  List<Pet> pets;
  bool cats = true;

  @override
  void initState() {
    pets = widget.pets;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(
                left: 15,
                right: 15,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: widget.onMenuTap,
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "Ubicación",
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                      Wrap(
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: Color(0x2f6062).withOpacity(1),
                            size: 12,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 3,
                            ),
                            child: Text(
                              "Boca de Camarioca",
                              style: TextStyle(fontSize: 11),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.grey,
                        radius: 14,
                      ),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(
                        top: 20,
                        bottom: 5,
                      ),
                      height: 50,
                      width: 300,
                      child: TextField(
                        onTap: () {},
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintText: 'Buscar',
                          contentPadding: EdgeInsets.all(12.0),
                          suffixIcon: Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(14.0),
                              borderSide: BorderSide.none),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 10,
                      ),
                      height: 65,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.symmetric(
                              horizontal: 15,
                            ),
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      pets = widget.pets
                                          .where((element) =>
                                              element.speciesId == 1)
                                          .toList();
                                      cats = true;
                                    });
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: cats
                                          ? Color(0x2f6062).withOpacity(1)
                                          : Colors.white,
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            blurRadius: 10,
                                            spreadRadius: 2,
                                            color: Color(0x2f6062)
                                                .withOpacity(0.1),
                                            offset: Offset(0, 2))
                                      ],
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 5,
                                    ),
                                    margin: const EdgeInsets.symmetric(
                                      vertical: 5,
                                    ),
                                    child: Image.asset(
                                      "assets/cat64.png",
                                    ),
                                  ),
                                ),
                                Text(
                                  "Cats",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                pets = widget.pets
                                    .where((element) => element.speciesId == 0)
                                    .toList();
                                cats = false;
                              });
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(
                                horizontal: 15,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: cats
                                          ? Colors.white
                                          : Color(0x2f6062).withOpacity(1),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            blurRadius: 10,
                                            spreadRadius: 2,
                                            color: Color(0x2f6062)
                                                .withOpacity(0.1),
                                            offset: Offset(0, 2))
                                      ],
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 5,
                                    ),
                                    margin: const EdgeInsets.symmetric(
                                      vertical: 5,
                                    ),
                                    child: Image.asset(
                                      "assets/dog64.png",
                                    ),
                                  ),
                                  Text(
                                    "Dogs",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: pets.length,
                        itemBuilder: (BuildContext context, int index) {
                          return PetTile(
                            pet: pets[index],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PetTile extends StatefulWidget {
  final Pet pet;
  PetTile({Key key, @required this.pet}) : super(key: key);

  @override
  _PetTileState createState() => _PetTileState();
}

class _PetTileState extends State<PetTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => PetDetailPage(
              pet: widget.pet,
            ),
          ),
        );
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        height: MediaQuery.of(context).size.width / 2,
        margin: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: 0,
              child: Container(
                height: MediaQuery.of(context).size.width * .45,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Card(
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 5,
                          ),
                          height: MediaQuery.of(context).size.width * .27,
                          width: MediaQuery.of(context).size.width - 40,
                          child: Row(
                            children: <Widget>[
                              Spacer(
                                flex: 1,
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          widget.pet.name,
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: <Widget>[
                                            Image.asset(
                                              widget.pet.gender
                                                  ? "assets/male.png"
                                                  : "assets/female.png",
                                              height: 15,
                                            ),
                                            Image.asset(
                                              widget.pet.speciesId == 1
                                                  ? "assets/cat64.png"
                                                  : "assets/dog64.png",
                                              height: 15,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Spacer(),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          widget.pet.breed,
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Spacer(),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "Edad: ${(DateTime.now().difference(widget.pet.birthDate).inDays / 365).toStringAsFixed(1)} años",
                                          style: TextStyle(
                                            fontSize: 11,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Spacer(),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(
                                          Icons.person,
                                          size: 16,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: Text(
                                            widget.pet.owner,
                                            style: TextStyle(
                                              fontSize: 11,
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              left: 0,
              child: Container(
                height: MediaQuery.of(context).size.width * .45,
                child: Card(
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: widget.pet.gender
                            ? Color(0xebd3af).withOpacity(1)
                            : Color(0xccd5da).withOpacity(1),
                      ),
                      height: MediaQuery.of(context).size.width * .45,
                      width: MediaQuery.of(context).size.width * .4,
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
