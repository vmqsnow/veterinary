import 'package:thalia/db/dbhelper.dart';
import 'package:thalia/models/pet.dart';

class HomeRepository {
  Future<List<Pet>> findAll() async {
    try {
      DBHelper<Pet> _dbHelper = new DBHelper<Pet>(
          dbTableName: 'pets', tableField: Pet.getFieldsForDB());

      var petsMap = await _dbHelper.findAll();
      List<Pet> pets = petsMap.map<Pet>((e) => Pet.fromJson(e)).toList();
      return pets;
    } catch (_) {
      return List();
    }
  }

  seedDatabase(List<Pet> pets) async {
    try {
      DBHelper dbHelper =
          DBHelper(dbTableName: "pets", tableField: Pet.getFieldsForDB());

      pets.forEach((element) async {
        dbHelper.saveOne(element.toMapForDB());
      });
    } catch (_) {
      return List();
    }
  }
}
