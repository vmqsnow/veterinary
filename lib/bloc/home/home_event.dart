import 'dart:async';
import 'package:meta/meta.dart';
import 'package:thalia/models/cache.dart';
import 'package:thalia/models/pet.dart';
import 'index.dart';

@immutable
abstract class HomeEvent {
  Stream<HomeState> applyAsync({HomeState state, HomeBloc bloc});

  final HomeRepository _homeRepository = HomeRepository();
}

class UnHomeEvent extends HomeEvent {
  @override
  Stream<HomeState> applyAsync({HomeState state, HomeBloc bloc}) async* {
    CacheController cacheController = CacheController();
    if (cacheController.pets == null || cacheController.needToUpdate) {
      List<Pet> pets = await _homeRepository.findAll();

      if (pets.length == 0) {
        pets = <Pet>[
          Pet(
              birthDate: DateTime.now(),
              breed: "Pequines",
              name: "Sochi",
              fixedphoneNumber: "45660034",
              gender: true,
              id: DateTime.now().millisecondsSinceEpoch,
              owner: "Armando (gordo)",
              phoneNumber: "54830981",
              speciesId: 1),
        ];
        _homeRepository.seedDatabase(pets);
      }

      cacheController.pets = pets;
      cacheController.needToUpdate = false;

      yield InHomeState(pets: pets);
    } else {
      yield InHomeState(pets: cacheController.pets);
    }
  }
}

class ReloadHomeEvent extends HomeEvent {
  @override
  Stream<HomeState> applyAsync({HomeState state, HomeBloc bloc}) async* {
    CacheController cacheController = CacheController();

    List<Pet> pets = await _homeRepository.findAll();

    cacheController.pets = pets;
    cacheController.needToUpdate = false;

    yield InHomeState(pets: pets);
  }
}
