import 'package:equatable/equatable.dart';
import 'package:thalia/models/pet.dart';
import 'package:flutter/material.dart';

abstract class HomeState extends Equatable {
  final List propss;
  HomeState([this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  HomeState getStateCopy();

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class UnHomeState extends HomeState {
  final String loadingMessage;

  UnHomeState({this.loadingMessage}) : super([loadingMessage]);

  @override
  String toString() => 'UnHomeState';

  @override
  UnHomeState getStateCopy() {
    return UnHomeState(loadingMessage: this.loadingMessage);
  }
}

/// Initialized
class InHomeState extends HomeState {
  List<Pet> pets;
  InHomeState({@required this.pets}) : super([pets]);

  @override
  String toString() => 'InHomeState';

  @override
  InHomeState getStateCopy() {
    return InHomeState(pets: pets);
  }
}

class ErrorHomeState extends HomeState {
  final String errorMessage;

  ErrorHomeState({@required this.errorMessage}) : super([errorMessage]);

  @override
  String toString() => 'ErrorHomeState';

  @override
  ErrorHomeState getStateCopy() {
    return ErrorHomeState(errorMessage: this.errorMessage);
  }
}
